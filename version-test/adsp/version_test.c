/****************************************************************************
 *   Copyright (c) 2016 James Wilson. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name ATLFlight nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

#include <memory.h>
//#include <dspal_version.h>
//#include "test_utils.h"

#ifdef __HEXAGON_ARCH__ 

/* Use the following macro for debug output on the aDSP. */
#include <HAP_farf.h>
#include <HAP_power.h>
//#include <HAP_vtcm_mgr.h>

/* Enable medium level debugging. */
//#define FARF_HIGH   1    /* Use a value of 0 to disable the specified debug level. */
//#define FARF_MEDIUM 1
//#define FARF_LOW    1

#define LOG_INFO(...) FARF(ALWAYS, __VA_ARGS__);
#define LOG_ERR(...) FARF(ALWAYS, __VA_ARGS__);
#define LOG_DEBUG(...) FARF(MEDIUM, __VA_ARGS__);

/*#ifdef __cplusplus
// Requires v3.3
extern "C" {
#endif

extern int HAP_query_total_VTCM(unsigned int* page_size, unsigned int* page_count);

#ifdef __cplusplus
}
#endif*/
#endif


int version_test_get_version_info(char* version_string, int version_stringLen, char* build_date_string, int build_date_stringLen, char* build_time_string, int build_time_stringLen)
{
	//dspal_get_version_info(version_string, build_date_string,
	//		       build_time_string);
	snprintf( version_string, version_stringLen, "hello world" );
	snprintf( build_date_string, build_date_stringLen, "%s", __DATE__);
	snprintf( build_time_string, build_time_stringLen,"%s", __TIME__ );
	LOG_INFO("Hello World" );
	LOG_INFO("build date: %s", __DATE__);
	LOG_INFO("build time: %s", __TIME__);

	//HAP_power_get_max_speed(int* clock_max, int* bus_max);
	int clock_max = 0;
	int bus_max = 0;
	int haprc = HAP_power_get_max_speed(&clock_max, &bus_max);
	LOG_INFO("rc: %d, clock_max: %d, bus_max: %d", haprc, clock_max, bus_max);

	return 0;
}
