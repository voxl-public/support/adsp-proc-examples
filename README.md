# aDSP (Apps DSP) Examples for VOXL

This project contains source code examples targeting the VOXL Applications DSP processor (aDSP). aDSP programs comprise of two components: aDSP libraries and CPU processor executable. CPU binaries produce corresponding FastRPC remote method invocations (RMI) from the ADSP libraries using an IDL-based interface.

Following this scheme, all sample projects comprise of an adsp and an apps component that must be build separately in their respective environments.

## Install Pre-requisites

```
$ git clone git@gitlab.com:voxl-public/voxl-docker.git
$ Download installers as instructed in "Install the voxl-hexagon Docker Image"
$ https://releases.linaro.org/archive/14.11/components/toolchain/binaries/arm-linux-gnueabihf/
$ cd voxl-docker
$ ./install-hexagon-docker.sh
$ voxl-docker -l | grep "voxl"
$ cp -r ~/.ssh/ ./ # Copy over ssh configs for git
$ voxl-docker -i voxl-hexagon
$ update git submodules, checkout 8096_adsp

```

## Download 

* Initialize repository submodules (including all sample dependencies) recursively before building sample projects.

```
$ git clone git@gitlab.com:voxl-public/adsp-proc-examples.git
$ cd adsp-proc-exampless
$ git submodule update --init --recursive
$ cd version-test
$ ./build.sh
$ ./deploy.sh
$ adb shell /home/root/version_test
```

You should see a result like the following:

```
version: hello world
build date: Jan 15 2020
build time: 22:47:39
```


